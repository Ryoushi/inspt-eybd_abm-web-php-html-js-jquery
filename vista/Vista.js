class Vista {
    controlador;

    divContenedorNavbar;
    divContenedorPpal;

    eventoClickActual;
    classNamePrev;
    eventTargetPrev;

    constructor() {
        this.divContenedorNavbar = "#contenedorNavbar";
        this.divContenedorPpal = "#contenedorPpal";
        console.log("Vista constructor");
    }

    iniciar() {
        this.cargaInicial();
        this.bindNavbarClick();
        this.bindFormSubmit();
        this.bindNavbarFocus();
        this.bindBodyClick();
    }

    setControlador(c) {
        this.controlador = c;
    }

    cargaInicial() {
        // necesario para acceder a Vista dentro de AJAX, puede usarse .bind(this) al final de las funciones y usar this 
        // el operador flecha no tiene su propio this y puede usarse para acceder a Vista
        var _this = this;
        // la función anónima es el callback llamado cuando el documento está listo
        // igual a: $(document).ready(function () { // o también el operador flecha: $(() => {
        $(function () {
            $(_this.divContenedorNavbar).load("vista/comunes/navbar.html");
            $(_this.divContenedorPpal).load("vista/inicio.html");
        });
    }

    // cambia el color del texto de los enlaces de la barra de navegación
    resaltarElementoActual() {
        var event = this.eventoClickActual;// guardado durante bindNavbarClick, cada vez que se cliquea en un enlace del navbar
        $(".active").removeClass("active");// quitar resaltado al anterior elemento cliqueado, de ser necesario
        $(event.target).addClass("active");// agregar resaltado al elemento cliqueado actual
    }

    // carga contenido de archivos html en divs de index, según href del anchor cliqueado
    // avisa a controlador y le pasa la ruta al contenido a cargar, para que el controlador decida donde cargarlo
    bindNavbarClick() {
        var contenedorNavbar = this.divContenedorNavbar;
        var controlador = this.controlador;
        var contenidoACargar;
        var _this = this;// necesario para acceder a this.eventoClickActual dentro del callback del método click

        $(function () {
            // navbar se carga después que Vista.js, $() debe tomar un elemento de index.html
            $(contenedorNavbar).on("click", function (event) {
                event.preventDefault();
                var idLink = event.target.id;
                switch (idLink) {
                    case 'dispListaLink':
                        _this.controlador.obtenerDispLista();
                        break;
                    case 'compListaLink':
                        _this.controlador.obtenerCompLista();
                        break;
                    default:
                        contenidoACargar = $(event.target).attr("href");
                        if (contenidoACargar) {// solo si hay un contenido a cargar (si se cliquea un enlace, tiene href)
                            _this.eventoClickActual = event;// debe ir antes de c.clickEnNavbar
                            controlador.clickEnNavbar(contenidoACargar);// llama a v.setContenedorPpal y resaltarElementoActual
                        }
                        break;
                }
//                console.log("En bindNavbarClick(), attr href: " + $(event.target).attr("href")
//                        + " contenidoACargar: " + contenidoACargar
//                        + " .href (property): " + event.target.href
//                        + " prop href: " + $(event.target).prop("href"));

//                controlador.probarModelo();
//                console.log("id: " + $("I").prop("id"));
            });
        });
    }

    // mostrar contenido dropdown cuando alguna parte del dropdown reciba foco mediante la tecla tab (no funciona con css)
    // no interviene el controlador
    bindNavbarFocus() {
        var contenedorNavbar = this.divContenedorNavbar;

        $(function () {
            $(contenedorNavbar).focusin(function (event) {
//                console.log("contenedorNavbar focusin en: " + event.target.href);
//                console.log("event.target.id " + event.target.id + "\nevent.target.class: " + event.target.className);
                $(".visible").removeClass("visible");
                if ($(event.target).hasClass("componentes")) {
                    $(".componentes").addClass("visible");
                } else if ($(event.target).hasClass("dispositivos")) {
                    $(".dispositivos").addClass("visible");
                }
            });
        });
//        console.log("método bindNavbarFocus");
    }
    // ocultar contenido dropdown cuando se hace clic en alguna parte de la página, permite ocultarlo después de tabular
    // *** FALTA (accesibilidad): navegación por teclado en los elementos del dropdown
    bindBodyClick() {
        $(function () {
            $("body").on("click", function (event) {
                $(".visible").removeClass("visible");
            });
        });
    }

    setContenedorPpal(href) {
        $(this.divContenedorPpal).load(href);
    }

    setContenedorNavbar(href) {
        $(this.divContenedorNavbar).load(href);
    }

    bindFormSubmit() {
        var contenedorPpal = this.divContenedorPpal;
//        var url;
        var idFormulario, formulario, formData;
        var _this = this;

        $(function () {// equivalente a $( document ).ready(function() {, ejecuta function cuando está listo el DOM
            $(contenedorPpal).submit(function (event) {
                event.preventDefault();
//                url = event.target.action;
//                console.log("En bindFormSubmit: url: " + url);// url: http://localhost/PhpDispCompExis/index.html
                console.log("En Vista.js, bindFormSubmit, event.target.id: " + event.target.id
                        + " tagName: " + event.target.tagName);

                idFormulario = event.target.id;
                formulario = event.target;
                formData = new FormData(event.target);

                switch (idFormulario) {
                    case 'loginInput':
                        _this.controlador.login(formData);
                        break;
                    case 'dispAltaInput':
                        _this.controlador.dispAlta(formData);
                        break;
                    case 'dispBajaInput':
                        _this.controlador.dispBaja(formData);
                        break;
                    case 'dispModificacionInput':
                        _this.controlador.dispModificacion(formData);
                        break;
                }
            });
        });
    }
}

console.log("Vista end");

//https://stackoverflow.com/questions/12498839/how-to-execute-php-code-within-javascript
//https://stackoverflow.com/questions/8988855/include-another-html-file-in-a-html-file
//https://www.geeksforgeeks.org/when-should-one-use-arrow-functions-in-es6/
//*1 https://api.jquery.com/val/

// Opcional: unobtrusive JavaScript - agregar head y body (incluyendo navbar) a archivos de Vista html, y cargar mediante jQuery solo un div o el body. Redireccionar sin JS?
// https://webplatform.github.io/docs/concepts/redirect_no_javascript/
// <noscript><meta http-equiv="refresh" content="0; url=www.example.com/no-js-version" /></noscript>
