<?php

session_start();

include 'Consola.php';
include 'Conector.php';
include 'ManejadorBD.php';

class Modelo {
    public $consola;
    public $conexion;
    private $manejadorBD;

    public function __construct() {
        $this->consola = new Consola();
        $conector = new Conector($this->consola);
        $this->conexion = $conector->crearConexion();
        $this->manejadorBD = new ManejadorBD($this->consola, $this->conexion);

        if($this->conexion){
            $this->consola->guardarBuffer("Conectado exitosamente\n");
        }
    }

    public function __destruct() {
        $this->conexion->close();
    }

    public function cerrarSesion() {
        ;
    }

    public function eliminarCookie($nombre) {
        if (isset($_COOKIE[$nombre])) {
            unset($_COOKIE[$nombre]);
            setcookie($nombre, '', time() - 3600, "/");
        }
    }

    public function probarCookie() {
        $count = count($_COOKIE);
        $cookieString = "$count cookies received.\n";
        foreach ($_COOKIE as $name => $value) {
            $cookieString = "  $name = $value\n";
        }
        $this->consola->guardarBuffer($cookieString);
    }

    public function crearConexion() {
        $conector = new conector();
        $this->conexion = $conector->crearConexion();
    }

    //  *** cambiar auth.php a objeto Autenticador.php, autenticar mediante Autenticador.php
    public function autenticar() {
//        $this->consola->guardarBuffer(json_encode($_POST['loginData']));
    }

    public function sanitizar($campo) {
        return filter_input(INPUT_POST, $campo, FILTER_SANITIZE_STRING);
    }

    public function dispAlta() {
        $campos = ['marca', 'modelo', 'funcion', 'linea', 'descripcion'];
        $datos = [];
        foreach ($campos as $campo) {
            $datos[$campo] = $this->sanitizar($campo);
        }
        $this->manejadorBD->dispAlta($datos);
    }

    public function dispBaja() {
        $this->manejadorBD->dispBaja();
    }

    public function dispModificacion() {
        $this->manejadorBD->dispModificacion();
    }

    public function dispListar() {
        $this->manejadorBD->dispListar();
    }
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $modelo = new Modelo();
    $accion = $modelo->sanitizar('accion');

    $modelo->consola->guardarBuffer("En Modelo.php, antes de switch, 'accion': $accion\n");

    switch ($accion) {
        case 'autenticar':
            $modelo->autenticar();
            break;
        case 'dispAlta':
            $modelo->dispAlta();
            break;
        case 'dispBaja':
            $modelo->dispBaja();
            break;
        case 'dispModificacion':
            $modelo->dispModificacion();
            break;
        case 'dispListar':
            $modelo->dispListar();
            break;
    }
    $modelo->consola->emitirBuffer();
} else {
    header("Location: ../index.html");
}

// autenticar, anterior
//if ($_SERVER['REQUEST_METHOD'] === 'POST') {
//    if ($_POST['accion'] === 'autenticar') {
//        $datos = $this->modelo->autenticar($_POST['loginData']);
//        echo json_encode($datos);
//    }
//}

// ***implementar cookies
//$modelo->probarCookie();
//echo("eliminar cookie\n");
//$modelo->eliminarCookie('PHPSESSID');
//echo("cookie eliminada\n");
//$modelo->probarCookie();
//echo "Echoing phpinfo:\n";
//echo phpinfo();
//if (isset($_COOKIE['PHPSESSID'])) {
//    echo "PHPSESSID cookie: " . $_COOKIE['PHPSESSID'];
//}
//
//    public function probar() {
//        $this->modeloProp++;
//        echo "probando función probar() en Modelo.php\n" . "variable \$modeloProp: " . $this->modeloProp;
//        return "return: función probar() en ModeloPHP.php\n ";
//    }
//
//    public function probar2() {
//        $this->modeloProp++;
//        if (!isset($_SESSION['testVar'])) {
//            $_SESSION['testVar'] = 0;
//        }
//        print "\$_SESSION[testVar]: $_SESSION[testVar]\n";
//        $_SESSION['testVar']++;
//    }
//
//print "\nprinting en Modelo";

// dispAlta si recibe JSON
        // los datos JSON no se convierten en array asociativo $_POST
//        $json_data = file_get_contents('php://input');
        // decodifica JSON en un array asociativo
//        $data = json_decode($json_data, true);
//        if ($data !== null) {
//            $marca = filter_var(data['marca'], FILTER_SANITIZE_STRING);
//            $modelo = filter_var(data['modelo'], FILTER_SANITIZE_STRING);
//            $funcion = filter_var(data['funcion'], FILTER_SANITIZE_STRING);
//            $linea = filter_var(data['linea'], FILTER_SANITIZE_STRING);
//            $desc = filter_var(data['desc'], FILTER_SANITIZE_STRING);
//        }
//
//        $this->manejadorBD->dispAlta($marca, $modelo, $funcion, $linea, $desc);

// dispAlta sin for_each
//        // recibir por POST y sanitizar las distintas entradas
//        $marca = filter_input(INPUT_POST, 'marca', FILTER_SANITIZE_STRING);
//        $modelo = filter_input(INPUT_POST, 'modelo', FILTER_SANITIZE_STRING);
//        $funcion = filter_input(INPUT_POST, 'funcion', FILTER_SANITIZE_STRING);
//        $linea = filter_input(INPUT_POST, 'linea', FILTER_SANITIZE_STRING);
//        $descripcion = filter_input(INPUT_POST, 'descripcion', FILTER_SANITIZE_STRING);
//        // unirlas en un array asociativo
//        $datos = array('marca' => $marca, 'modelo' => $modelo, 'funcion' => $funcion, 'linea' => $linea, 'descripcion' => $descripcion);
//        //enviar el array a manejadorBD
        
// *** usar pool de conexiones o singleton (solo instancias únicas de c/objeto) para evitar impacto en rendimiento al abrir y cerrar conexiones
//$modelo->crearConexion();
