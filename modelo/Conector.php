<?php

class Conector {

    private $consola;
    private $servidor;// var son public. var alternativa no recomendada a public (compatibilidad) *1
    private $usuario;
    private $password;
    private $conexion;

    public function __construct($cons) {
        $this->consola = $cons;
        $this->servidor = "localhost";
        $this->usuario = "root";
        $this->password = "";
        $this->crearConexion();
    }

    public function crearConexion() {
        $this->conexion = new mysqli($this->servidor, $this->usuario, $this->password);
        $this->conexion->query("SET NAMES 'utf8'");

        if ($this->conexion->connect_error) {// Nota: $connect_error requiere PHP < 5.2.9 *2
            die("Falló la conexión: " . $this->conexion->connect_error);
        }
//        echo "Conectado exitosamente\n";

        return $this->conexion;
    }
}

// https://stackoverflow.com/questions/3961456/difference-between-double-colon-and-arrow-operators-in-php
// https://www.php.net/manual/es/language.oop5.visibility.php
// https://www.w3schools.com/php/php_mysql_connect.asp
// *1 https://www.php.net/manual/es/language.oop5.properties.php
// *2 https://www.w3schools.com/php/php_mysql_connect.asp
