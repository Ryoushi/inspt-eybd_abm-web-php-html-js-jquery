class Modelo {
    constructor() {
    }

// /modelo toma localhost como raíz, modelo (relativo) se ubica en PhpDispCompExis
    pruebaPHPAjax() {
        $(function () {
            $.ajax({
                url: "modelo/Modelo.php",
                data: {testKey: "test123"},
                success: function (abc) {
                    if (abc) {
                        console.log("pruebaPHPAjax: En Modelo.js, de Modelo.php: " + abc);
                    } else {
                        console.log("falló pruebaPHPAjax");
                    }
                }
            });
        });
    }

    getArchivo(nombre) {

    }

    autenticar(loginData) {
        loginData.append('accion', 'autenticar');
        console.log("En Modelo.js, autenticar, loginData: " + loginData +
                "\nloginData.user: " + loginData.user +
                "\nloginData.pass: " + loginData.pass);

//        $.post("modelo/auth.php", {accion: 'autenticar', loginData}, function (dato) {
//        $.post("modelo/Modelo.php", {accion: 'autenticar', loginData: loginData}, function (dato) {
//        $.post("modelo/Modelo.php", loginData, function (dato) {// loginData sin clave queda asociado a 'loginData', aunque no sea JSON válido
//            console.log("En Modelo.js, 'dato' recibido: " + dato + "\nFin 'dato' recibido");
//        });

        $.ajax({
            url: "modelo/Modelo.php",
            type: "POST",
            data: loginData,
            processData: false,
            contentType: false,
            success: function (data, textStatus, jqXHR) {
                console.log("En Modelo.js, autenticar success, data: " + data
                        + " textStatus: " + textStatus + " jqXHR: " + jqXHR);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("En Modelo.js, autenticar error, jqXHR: " + jqXHR + " textStatus: " + textStatus + " errorThrown: " + errorThrown);
            }
        });

// Código anterior, $.ajax con type: "POST", equivalente a $.post, y 
// $(function () innecesario si no se necesita esperar a la carga completa del DOM, y ya fue incluido en Vista.js
//        $(function () {
//            $.ajax({
//                url: "modelo/Modelo.php",
//                data: loginData,
//                success: function (abc) {
//                    if (abc) {
//                        console.log(abc);
//                    } else {
//                        console.log("falló pruebaPHPAjax");
//                    }
//                }
//            });
//        });
    }

// *** consolidar en uno. Recordar que datos ya es FormData, agregar datos mediante append
    dispAlta(datos) {
//        tipo de dato correcto
//        console.log("en Modelo.js tipo de datos: " + typeof datos + " formData: " + datos);
        datos.append('accion', 'dispAlta');
//        console.log("en Modelo.js tipo de datos: " + typeof datos + " formData: " + datos);

// verificando contenido de FormData datos, correcto
//        for (let pair of datos.entries()) {
//            console.log(pair[0] + ', ' + pair[1]);
//        }

//        $.post('Modelo.php', datos, function (respuesta) {
//        $.post('Modelo.php', {accion: 'dispAlta', datos: datos}, function (respuesta) {
//            console.log("En Modelo.js, 'respuesta' 'dispAlta' recibida: " + respuesta 
//                    + "\nFin 'respuesta' 'dispAlta' recibida");
//        });

        // processData: false es necesario para poder enviar FormData sin que se transforme en un query string *1
        // Sin contentType:false, falla sin error. Sin processData: false:
        // Uncaught TypeError: 'append' called on an object that does not implement interface FormData.
        $.ajax({
            url: "modelo/Modelo.php",
            type: "POST",
            data: datos,
            processData: false,
            contentType: false,
            success: function (data, textStatus, jqXHR) {
                console.log("En Modelo.js, dispAlta success, data: " + data
                        + " textStatus: " + textStatus + " jqXHR: " + jqXHR);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("En Modelo.js, dispAlta error, jqXHR: " + jqXHR + " textStatus: " + textStatus + " errorThrown: " + errorThrown);
            }
        });
    }
    dispBaja(datos) {
        datos.append('accion', 'dispBaja');
        $.post('Modelo/Modelo.php', datos, function (respuesta) {
            console.log("En Modelo.js, 'respuesta' 'dispBaja' recibida: " + respuesta
                    + "\nFin 'respuesta' 'dispBaja' recibida");
        });
    }
    dispModificacion(datos) {
        datos.append('accion', 'dispModificacion');
        $.post('Modelo/Modelo.php', datos, function (respuesta) {
            console.log("En Modelo.js, 'respuesta' 'dispModificacion' recibida: " + respuesta
                    + "\nFin 'respuesta' 'dispModificacion' recibida");
        });
    }
    dispListar() {
        $.post('Modelo/Modelo.php', {accion: 'dispListar'}, function (respuesta) {
            try {
                let dato = JSON.parse(respuesta);
                console.log(dato);// con .toString o en medio de una cadena, imprime [object Object]
                console.log(dato['dato']);// dato como array listo para usar
                console.log("En Modelo.js, 'respuesta' de accion 'dispListar' recibida---\n" + JSON.stringify(dato, null, 4)
                        + "\n---Fin 'respuesta' 'dispListar' recibida");
                return dato;
            } catch (error) {
                console.error('En Modelo.js, dispListar: Error al analizar JSON:', error);
            }
        });
    }
}

// *1 https://stackoverflow.com/questions/25390598/append-called-on-an-object-that-does-not-implement-interface-formdata
// *1 https://api.jquery.com/jQuery.ajax/
