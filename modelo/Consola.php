<?php

// similar a output buffer ob_*, permite acumular salidas y emitirlas cuando se necesite
// aunque en un objeto JSON, con clase y función llamadora como clave única
class Consola {

    private $outputArray;

    public function __construct() {
        $this->outputArray = array();
    }

//    public function log($output, $with_script_tags = true) {
//        $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) . ');';
//        if ($with_script_tags) {
//            $js_code = '<script>' . $js_code . '</script>';
//        }
//        echo $js_code;
//    }

    // __CLASS__-__FUNCTION__ para saber de dónde proviene el mensaje emitido al final de la ejecución PHP
    private function crearClave() {
        $clave;
        $traza = debug_backtrace();
        if (count($traza) > 2) {
            $trazaTres = $traza[2]; // [int] indica el método de la pila de llamadas que se quiere obtener
            $clave = $trazaTres['class'] . "-" . $trazaTres['function'];
        } else {
            $clave = "global-" . count($this->outputArray);// contexto global no es registrado en la pila de llamadas
        }
        return $clave;
    }
    public function guardarBuffer($valor) {
        $this->outputArray[$this->crearClave()] = $valor;
    }

    // Usar este método para guardar un dato relevante (no mensajes trace) por ejecución PHP en métodos tipo Listar(consulta)
    // Extraer el dato como valor asociado a la clave "dato". Requiere que emitirBuffer devuelva un JSON (par clave-valor)
    public function guardarDatoBuffer($valor) {
        $this->outputArray["dato"] = $valor;
    }

    public function emitirBuffer() {
        echo json_encode($this->outputArray);
    }
}
