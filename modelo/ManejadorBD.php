<?php

class ManejadorBD {

    private $consola;
    private $conexion;
    private $bd;
    private $tablasSql;

    public function __construct(&$cons, $conx) {
        $this->consola = &$cons;
        $this->conexion = $conx;
        $this->bd = "dce";// Nombre de la base de datos a crear y utilizar
        $this->tablasSql = [// SQL de tablas a crear y utilizar
            "usuarios" => "CREATE TABLE IF NOT EXISTS `usuarios` (`id` INT NOT NULL AUTO_INCREMENT , `nombre` TEXT NOT NULL , `password` TEXT NOT NULL , `tipo` TEXT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;",
            "dispositivos" => "CREATE TABLE IF NOT EXISTS `dispositivos` (`id` INT NOT NULL AUTO_INCREMENT , `marca` TEXT NOT NULL , `modelo` TEXT NOT NULL , `funcion` TEXT NOT NULL , `linea` TEXT NULL , `descripcion` TEXT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;",
            "componentes" => "CREATE TABLE IF NOT EXISTS `componentes` (`id` INT NOT NULL AUTO_INCREMENT , `marca` TEXT NOT NULL , `modelo` TEXT NOT NULL , `funcion` TEXT NOT NULL , `linea` TEXT NULL , `descripcion` TEXT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;",
            "dispositivos_componentes" => "CREATE TABLE IF NOT EXISTS `dispositivos_componentes` (`id_dispositivos` INT NOT NULL , `id_componentes` INT NOT NULL , `nota` TEXT NULL , PRIMARY KEY (`id_dispositivos`, `id_componentes`), FOREIGN KEY (`id_dispositivos`) REFERENCES `dispositivos`(`id`), FOREIGN KEY (`id_componentes`) REFERENCES `componentes`(`id`)) ENGINE = InnoDB;",
        ];// ENGINE elige un motor de almacenamiento para la tabla, no se recomienda cambiarlo después si ya contiene datos

        $this->crearBD($this->bd);
        $this->elegirBD($this->bd);
        $this->crearTablas($this->tablasSql);
    }

    public function crearBD($bd) {// *1
        $sql = "CREATE DATABASE IF NOT EXISTS $bd";

        if ($this->conexion->query($sql) === TRUE) {
            $this->consola->guardarBuffer("Base de datos creada correctamente: $bd\n");
        } else {
            $this->consola->guardarBuffer("Error al crear la base de datos: $this->conexion->error\n");
        }
    }

    private function elegirBD($bd) {
        if ($this->conexion->select_db($bd) === TRUE) {
            $this->consola->guardarBuffer("Base de datos elegida: $bd.\n");
        } else {
            $this->consola->guardarBuffer("Error al elegir la base de datos: $this->conexion->error\n");
        }
    }

    public function crearTablas($tablasSql) {
        foreach ($tablasSql as $sql) {
            if ($this->conexion->query($sql) === TRUE) {
                $this->consola->guardarBuffer("Tabla creada correctamente.\n");
            } else {
                $this->consola->guardarBuffer("Error al crear la tabla: $conexion->error\n");
            }
        }
    }

    public function dispListar() {
        $sql = "SELECT * FROM `dispositivos`";

        $stmt = $this->conexion->prepare($sql);
        $stmt->execute();
        $res = $stmt->get_result();
        $resArray = array();
        while ($fila = $res->fetch_assoc()) { // devuelve null cuando no hay más filas
            $resArray[] = $fila;                    // agregar elementos a array
        }
        $this->consola->guardarDatoBuffer($resArray);
    }

    public function dispAlta($datos) {
        $sql = "INSERT INTO dispositivos (marca, modelo, funcion, linea, descripcion) VALUES (?, ?, ?, ?, ?)";

        $stmt = $this->conexion->prepare($sql);
        if ($stmt === false) {
            $this->consola->guardarBuffer("Error en la preparación de la consulta: alta dispositivos $conexion->error");
        }
        $stmt->bind_param("sssss", $datos['marca'], $datos['modelo'], $datos['funcion'], $datos['linea'], $datos['descripcion']);
        if ($stmt->execute() === false) {
            $this->consola->guardarBuffer("Error al ejecutar la consulta alta dispositivos: $stmt->error");
        } else {
            $this->consola->guardarBuffer("Se insertaron los datos correctamente.");
        }
    }

    public function compAlta($datos) {
        $sql = "INSERT INTO componentes (marca, modelo, funcion, linea, descripcion) VALUES (?, ?, ?, ?, ?)";

        $stmt = $this->conexion->prepare($sql);
        if ($stmt === false) {
            $this->consola->guardarBuffer("Error en la preparación de la consulta alta componentes: $conexion->error");
        }
        $stmt->bind_param("sssss", $datos['marca'], $datos['modelo'], $datos['funcion'], $datos['linea'], $datos['descripcion']);
        if ($stmt->execute() === false) {
            $this->consola->guardarBuffer("Error al ejecutar la consulta alta componentes: $stmt->error");
        } else {
            $this->consola->guardarBuffer("Se insertaron los datos correctamente.");
        }
    }
}

// *1 https://stackoverflow.com/questions/28544617/mysqli-connect-withtout-specifying-a-database