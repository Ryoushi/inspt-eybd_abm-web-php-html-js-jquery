class Controlador {
    modelo;
    vista;

    constructor(m, v) {
        console.log("Controlador constructor");
        this.modelo = m;
        this.vista = v;
    }

    /*** Métodos de Vista ***/

    iniciarVista() {
        this.vista.setControlador(this);
        this.vista.iniciar();// cargar divs vacíos en index.html con comunes/navbar.html e inicio.html
    }

    clickEnNavbar(contenidoACargar) {
        this.vista.setContenedorPpal(contenidoACargar);
        this.vista.resaltarElementoActual();
    }

    /*** Métodos de Modelo ***/

    probarModelo() {
        this.modelo.pruebaPHPAjax();
    }

    printFile(nombre) {
        this.modelo.getArchivo(nombre);
    }

    autenticar(loginData) {
        this.modelo.autenticar(loginData);
    }

    // depende de que Vista envíe datos FormData y que Modelo reciba POST. Para evitar dependencia, transformar en JSON antes

    login(datos) {
        this.modelo.autenticar(datos);
    }

    dispAlta(datos) {
        this.modelo.dispAlta(datos);
    }
    dispBaja(datos) {
        this.modelo.dispBaja(datos);
    }
    dispModificacion(datos) {
        this.modelo.dispModificacion(datos);
    }
    obtenerDispLista() {
        let dispLista = this.modelo.dispListar();
        console.log("Controlador.js, obtenerDispLista, dispLista: " + dispLista);
        return dispLista;
    }

    compAlta(datos) {
        this.modelo.compAlta(datos);
    }
    compBaja(datos) {
        this.modelo.compBaja(datos);
    }
    compModificacion(datos) {
        this.modelo.compModificacion(datos);
    }
    obtenerCompLista() {
        let compLista = this.modelo.compListar();
        console.log("Controlador.js, obtenerCompLista, dispLista: " + compLista);
        return compLista;
    }
}

modelo = new Modelo();
vista = new Vista();
controlador = new Controlador(modelo, vista);

controlador.iniciarVista();

console.log('Controlador end');

//https://stackoverflow.com/questions/12498839/how-to-execute-php-code-within-javascript
// https://stackoverflow.com/questions/51985920/add-javascript-object-to-form-data-while-submit-the-form
