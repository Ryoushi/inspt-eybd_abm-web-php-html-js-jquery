<?php

include_once("modelo/Modelo.php");
include_once("vista/Vista.php");

class Controlador {

    private $modelo;
    private $vista;
    private $actual;

    public function __construct() {
        $this->modelo = new Modelo();
        $this->vista = new Vista();
        $this->actual = 'i';
    }

    public function invocar() {                                         // https://stackoverflow.com/questions/19767894/warning-do-not-access-superglobal-post-array-directly-on-netbeans-7-4-for-ph
//        if (null !== filter_input(INPUT_POST, 'disp', FILTER_SANITIZE_STRING)){                                     // filter_input(INPUT_SERVER, 'SERVER_NAME', FILTER_SANITIZE_STRING)
//            $disp = $this->modelo->getDispLista();
//        }
        $this->vista->verMenu();
        switch ($this->actual){
            case "i":
                $this->vista->verIndex();   // 1ra página, index
                break;
            case "l":
                $this->vista->verLogIn();
                break;
            default:
                $this->vista->verIndex();
                break;
                
        }
        
//        $this->vista->verLogIn();
    }
    public function verDispAlta(){
        echo "test";
    }

}
